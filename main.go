package main

import (
	"bufio"
	"bytes"
	"io/ioutil"
	"log"
	"strings"

	"9fans.net/go/acme"
)

func main() {
	l, err := acme.Log()
	if err != nil {
		log.Fatal(err)
	}

	for {
		event, err := l.Read()
		if err != nil {
			log.Fatal(err)
		}
		if event.Name != "" && event.Op == "put" && strings.HasSuffix(event.Name, ".rb") {
			if err := stripTrailingWhitespace(event.ID, event.Name); err != nil {
				log.Print(err)
			}
		}
	}
}

func stripTrailingWhitespace(id int, name string) error {
	w, err := acme.Open(id, nil)
	if err != nil {
		return err
	}
	defer w.CloseFiles()

	oldContent, err := ioutil.ReadFile(name)
	if err != nil {
		return err
	}

	newContent := make([]byte, 0, len(oldContent))
	scanner := bufio.NewScanner(bytes.NewReader(oldContent))
	lastChanged := 0
	for line := 0; scanner.Scan(); line++ {
		newLineContent := bytes.TrimRight(scanner.Bytes(), " ")
		if !bytes.Equal(newLineContent, scanner.Bytes()) {
			lastChanged = line
		}
		newContent = append(append(newContent, newLineContent...), '\n')
	}
	if err := scanner.Err(); err != nil {
		return err
	}

	if bytes.Equal(oldContent, newContent) {
		return nil
	}

	if err := w.Ctl("mark"); err != nil {
		return err
	}
	if err := w.Ctl("nomark"); err != nil {
		return err
	}
	if err := w.Addr(","); err != nil {
		return err
	}

	if _, err := w.Write("data", newContent); err != nil {
		return err
	}

	if err := w.Addr("%d/$/", lastChanged); err != nil {
		return err
	}
	if err := w.Ctl("dot=addr"); err != nil {
		return err
	}
	if err := w.Ctl("show"); err != nil {
		return err
	}

	return nil
}
